<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = ['user_id', 'question_id', 'post_id', 'parent_id', 'hidden_author', 'content'];

    protected $hidden = ['hidden_author', 'removed'];

    public function user() {
        return $this->belongsTo(User::class);
    }

}
