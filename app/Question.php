<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'title', 'content', 'slug', 'image', 'disable_comments', 'disable_man', 'disable_woman',
        'disable_anonymous', 'created_at', 'updated_at', 'user', 'category',
        'short_content', 'category_data', 'editor_picked', 'hidden_author', 'content_type'
    ];

    protected $hidden = [
        'removed', 'hidden_author'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
