<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLike extends Model
{
    protected $table = 'user_likes';

    protected $fillable = [
        'post_id',
        'question_id',
        'comment_id',
        'user_id',
        'liked',
        'dissliked'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
