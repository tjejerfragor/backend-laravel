<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Lang;

class RegistrationMail extends Mailable
{
    use Queueable, SerializesModels;

    private $id;
    private $hash;

    /**
     * RegistrationMail constructor.
     * @param string $id
     * @param string $hash
     */
    public function __construct(string $id, string $hash)
    {
        $this->id = $id;
        $this->hash = $hash;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('RegistrationMail')
    }
}
