<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    protected $table = 'post_likes';

    protected $fillable = [
        'female_like_count',
        'female_disslike_count',
        'male_like_count',
        'male_disslike_count',
        'post_id',
        'question_id',
        'comment_id'
    ];


    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }
}
