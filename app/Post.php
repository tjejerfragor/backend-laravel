<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'title', 'content', 'slug', 'image', 'disable_comments', 'disable_man', 'disable_woman',
        'disable_anonymous', 'created_at', 'updated_at', 'user', 'category',
        'short_content', 'category_data', 'editor_picked'
    ];

    protected $hidden = [
        'removed',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
