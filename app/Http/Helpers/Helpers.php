<?php


namespace App\Http\Helpers;

use Illuminate\Http\Request;

class Helpers
{
    public static function handleImageUpload(Request $request)
    {
        if ($request->hasFile('image')) {
            // Get filename with extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('image')->storeAs('public/images/posts', $fileNameToStore);

        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        return $fileNameToStore;
    }
}
