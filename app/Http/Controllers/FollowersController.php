<?php

namespace App\Http\Controllers;

use App\Follower;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class FollowersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     * @param $request Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userId = Auth::user()->id;
        $requestedUserId = $request->get('userId');

        if ($request->has('userId')) {
            return Follower::with(['user', 'actor'])
                ->where('user_id', $requestedUserId)
                ->paginate(env('PAGINATION_LIMIT'));
        }

        $myFollowers = Follower::with(['user', 'actor'])
            ->where('user_id', $userId)
            ->paginate(env('PAGINATION_LIMIT'));

        return $myFollowers;


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userId = User::where('hidden_profile', false)->where('id', $id)->firstOrFail()->id;
        $myFollowers = Follower::with(['user', 'actor'])->where('user_id', $userId)->firstOrFail();

        return $myFollowers;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $actorId = $request->has('actor') ? $request->post('actor') : null;

        if ($validator = $this->validator($request)) {
            // Checking if user already follows actor, if not follow it.
            if (!$this->checkIfUserFollowsActor($userId, $actorId)) {
                $newFollower = Follower::create([
                    'user_id' => $userId,
                    'actor_id' => $actorId
                ]);

                return response()->json([
                    'status' => true,
                    'status_code' => 'followed'
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'status_code' => 'already_following'
                ], 200);
            }
        } else {
            return response()->json([
                'status' => false,
                'errors' => $validator
            ]);
        }
    }

    /**
     * Validate before storing anyting
     * @param $request Request
     */
    private function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'actor' => 'required|exists:users,id'
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return true;

    }

    /**
     * @param int $userId - User id to check if follows actor
     * @param int $actorId - actor id to check if user follows actor.
     * @return bool
     */
    private function checkIfUserFollowsActor(int $userId, int $actorId)
    {
        return Follower::where('user_id', $userId)->where('actor_id', $actorId)->exists();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $request Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $userId = Auth::user()->id;
        $actorId = $request->has('actor') ? $request->get('actor') : null;
        Log::emergency('The system is down!');

        if ($validator = $this->validator($request)) {
            // Checking if user already follows actor, if not follow it.
            if ($this->checkIfUserFollowsActor($userId, $actorId)) {
                try {
                    Follower::where('user_id', $userId)->where('actor_id', $actorId)->delete();
                } catch (\Exception $e) {
                    Log::error('Cant delete the followers');
                }

                return response()->json([
                    'status' => true,
                    'status_code' => 'followed'
                ], 200);
            }

        } else {
            return response()->json([
                'status' => false,
                'errors' => $validator
            ]);
        }
    }
}
