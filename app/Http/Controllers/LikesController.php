<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\PostLike as Likes;
use App\Question;
use App\UserLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LikesController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:api', 'throttle:1,1'])->only(['likeEntry', 'dissLikeEntry']);
    }

    /**
     * Display a listing of the resource.
     * @param $request Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('question')) {
            $post = Likes::where('question_id', $request->get('question'))->firstOrFail();
            return $this->show($post->id);
        }

        if ($request->has('comment')) {
            $post = Likes::where('comment_id', $request->get('comment'))->firstOrFail();
            return $this->show($post->id);
        }

        if ($request->has('post')) {
            $post = Likes::where('post_id', $request->get('post'))->firstOrFail();
            return $this->show($post->id);
        }


        $likes = Likes::paginate(env('PAGINATION_LIMIT'));

        return $likes;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Likes::findOrFail($id);
    }

    /**
     * Show the form for creating a new resource.
     * @param $id int
     * @param $request \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function likeEntry(Request $request, int $id)
    {
        $questionId = $request->post('isQuestion');
        $postId = $request->post('isPost');
        $commentId = $request->post('isComment');
        $userId = Auth::user()->id;
        $exists = null;
        $gender = $request->post('gender');
        $user_like = null;
        $validator = $this->likeEntryValidator($request);
        $dataToBeSaved = [
            'user_id' => $userId,
            'liked' => True
        ];

        if ($validator === true) {
            if ($request->has('isQuestion')) {
                $user_like = UserLike::where('question_id', $questionId);
                $dataToBeSaved['question_id'] = $questionId;
            }
            if ($request->has('isPost')) {
                $user_like = UserLike::where('post_id', $postId);
                $dataToBeSaved['isPost'] = $postId;
            }
            if ($request->has('isComment')) {
                $user_like = UserLike::where('comment_id', $commentId);
                $dataToBeSaved['isComment'] = $commentId;
            }
            if (!$user_like->exists()) {

                // Increase post/question/comment like count.
                $postLikeEntry = Likes::findOrFail($id);
                if (!$gender === 'woman') {
                    $postLikeEntry->male_like_count = $postLikeEntry->male_like_count + 1;
                } else {
                    $postLikeEntry->female_like_count = $postLikeEntry->female_like_count + 1;
                }
                $postLikeEntry->save();

                // Create a userlike entry to know that user has already liked this post/question/comment.
                $user_like = new UserLike($dataToBeSaved);
                $user_like->save();
                echo $postLikeEntry->female_like_count;
                return response()->json([
                    'status' => true
                ]);
            } else {
                //Revoke the like action
                $dataToBeSaved['liked'] = false;
                $user_like = new UserLike($dataToBeSaved);
                $user_like->save();
                return response()->json([
                    'status' => false
                ]);
            }


        } else {

            return response()->json([
                'status' => false,
                'errors' => $validator
            ]);
        }
    }

    private function likeEntryValidator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'isQuestion' => 'required_without_all:isPost,isComment|exists:questions,id',
            'isPost' => 'required_without_all:isQuestion,isComment,|exists:posts,id',
            'isComment' => 'required_without_all:isPost,isQuestion|exists:comments,id',
            'gender' => 'required|in:man,woman'
        ]);

        if ($request->has('isQuestion') && $request->has('isPost') && $request->has('isComment')) {
            return response()->json([
                'error' => '?'
            ]);
        }

        if ($validator->fails()) {
            return $validator->errors();
        }

        return true;
    }

    public function dissLikeEntry(Request $request, int $id)
    {
        $questionId = $request->post('isQuestion');
        $postId = $request->post('isPost');
        $commentId = $request->post('isComment');
        $userId = Auth::user()->id;
        $exists = null;
        $gender = $request->post('gender');
        $user_like = null;
        $validator = $this->likeEntryValidator($request);
        $dataToBeSaved = [
            'user_id' => $userId,
        ];

        if ($validator === true) {
            if ($request->has('isQuestion')) {
                $user_like = UserLike::where('question_id', $questionId)->where('user_id', $userId);
                $dataToBeSaved['question_id'] = $questionId;
            }
            if ($request->has('isPost')) {
                $user_like = UserLike::where('post_id', $postId)->where('user_id', $userId);
                $dataToBeSaved['isPost'] = $postId;
            }
            if ($request->has('isComment')) {
                $user_like = UserLike::where('comment_id', $commentId)->where('user_id', $userId);
                $dataToBeSaved['isComment'] = $commentId;
            }

            if ($user_like->exists() && $user_like->first()->dissliked != true) {

                // Increase post/question/comment like count.
                $postLikeEntry = Likes::findOrFail($id);
                if (!$gender === 'woman') {
                    $postLikeEntry->male_like_count = $postLikeEntry->male_like_count - 1;
                } else {
                    $postLikeEntry->female_like_count = $postLikeEntry->female_like_count - 1;
                }
                $postLikeEntry->save();

                // Create a userlike entry to know that user has already liked this post/question/comment.
                $user_like = UserLike::where($dataToBeSaved)->firstOrFail();
                $user_like->liked = false;
                $user_like->dissliked = true;
                $user_like->save();
                echo $postLikeEntry->female_like_count;
                return response()->json([
                    'status' => true
                ]);
            } else {

                //Revoke the disslike action
                $user_like = UserLike::where($dataToBeSaved)->firstOrFail();
                $user_like->liked = false;
                $user_like->dissliked = false;
                $user_like->save();
                return response()->json([
                    'status' => false
                ]);
            }


        } else {

            return response()->json([
                'status' => false,
                'errors' => $validator
            ]);
        }
    }

}
