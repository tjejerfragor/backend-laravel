<?php

namespace App\Http\Controllers;

use App\Post;
use App\Question;
use Illuminate\Http\Request;

class AllEntries extends Controller
{
    public static function index(Request $request) {
        $posts = Post::paginate(env('PAGINATION_LIMIT'));
        $questions = Question::paginate(env('PAGINATION_LIMIT'));

        return response()->json([
            'posts' => $posts,
            'questions' => $questions->pluck('data')
        ]);
    }
}
