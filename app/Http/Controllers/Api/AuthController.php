<?php

namespace App\Http\Controllers\Api;

use App\Mail\RegistrationMail;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public $successStatus = 200;

    public function __construct()
    {
        $this->middleware('auth:api')->only('verifyEmail');
    }

    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required|unique:users,username',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'password_verify' => 'required|same:password',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        if (Auth::attempt(['email' => $request->post('email'), 'password' => $request->post('password')])) {
            event(new Registered($user));
            $verificationString = $this->generateVerificationString($request);
            $this->sendVerificationString($request, $verificationString['id'], $verificationString['hash']);
        }

        $success['token'] = $user->createToken('Tjejerfrågar')->accessToken;
        return response()->json(['status' => true, "token" => $success['token']], $this->successStatus);
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('Tjejerfrågar')->accessToken;
            return response()->json(['status' => "success", "token" => $success['token']], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function getUser(Request $request)
    {
        $user = Auth::user();

        return response()->json([
            "user" => $user
        ]);
    }

    public function verifyEmail(Request $request, $id, $hash)
    {
        if (!hash_equals((string)$request->route('id'), (string)$request->user()->getKey())) {
            throw new AuthorizationException;
        }

        if (!hash_equals((string)$request->route('hash'), sha1($request->user()->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if ($request->user()->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }
        echo 'qwe';
        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    private function generateVerificationString(Request $request)
    {
        $url = URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $request->user()->getKey(),
                'hash' => sha1($request->user()->getEmailForVerification()),
            ]
        );


        $id = explode('/', $url)[6];
        $hash = explode('/', $url)[7];

        return [
            'id' => $id,
            'hash' => $hash
        ];

    }

    /**
     * @param Request $request
     * @param string $id
     * @param string $key
     * @return MailMessage
     */
    private function sendVerificationString(Request $request, string $id, string $hash)
    {

        $mail =  (new MailMessage)
            ->subject(Lang::get('Verify Email Address'))
            ->line(Lang::get('Please click the button below to verify your email address.'))
            ->action(Lang::get('Verify Email Address'), hash('sha1', (string)$id, (string)$hash))
            ->line(Lang::get('If you did not create an account, no further action is required.'));

        Mail::to($request->user()->email)->send(new RegistrationMail($id, $hash));

    }
}
