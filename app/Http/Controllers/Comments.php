<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use function foo\func;

class Comments extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'destroy', 'update']);
    }

    /**
     * Display a listing of the resource.
     * @param $request Illuminate\Http\Reques
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('question')) {
            $post = Comment::where('question_id', $request->get('question'))->firstOrFail();
            return Comment::with('user')
                ->where('removed', '!=', true)
                ->where('question_id', $post->id)
                ->paginate(15);
        }

        if ($request->has('parent')) {
            $post = Comment::where('parent_id', $request->get('parent'))->firstOrFail();
            return Comment::with('user')
                ->where('removed', '!=', true)
                ->where('parent_id', $post->id)
                ->paginate(15);
        }

        if ($request->has('post')) {
            $post = Comment::where('post_id', $request->get('post'))->firstOrFail();
            return Comment::with('user')
                ->where('removed', '!=', true)
                ->where('post_id', $post->id)
                ->paginate(15);
        }
        if ($request->has('user')) {
            $post = Comment::where('user_id', $request->get('user'))->firstOrFail();
            return Comment::with('user')
                ->where('removed', '!=', true)
                ->where('user_id', $post->id)
                ->paginate(15);
        }

        return Comment::with('user')->where('removed', '!=', true)->paginate(15);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Comment::with('user')->where('removed', '!=', true)->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validator($request);
        if ($validated === true) {
            $entryType = $request->has('question') ? 'question_id' : 'post_id';
            $entryId = ($request->has('question')) ? $request->post('question') : $request->post('post');

            $comment = new Comment();
            $comment->user_id = Auth::user()->id;
            $comment->content = $request->get('content');
            $comment->$entryType = intval($entryId);
            $comment->save();

            return $comment::with('user')->findOrFail($comment->id);
        } else {
            return response()->json([
                'status' => false,
                'errors' => $validated
            ]);
        }
    }

    private function validator(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'content' => 'required|min:15|max:200',
            'question' => 'integer|exists:questions,id',
            'post' => 'integer|exists:posts,id',
        ]);


        if ($validate->fails()) {
            if ($request->has('question') && $request->has('post')) {
                return false;
            }
            return $validate->errors();
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::where('id', $id)->update([
            'removed' => true
        ]);
    }
}
