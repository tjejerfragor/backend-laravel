<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Helpers;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Response;

class Questions extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'destroy', 'update']);
    }

    /**
     * Display a listing of the resource.
     * @param $request Request
     * @return Response
     */
    public function index(Request $request)
    {
        $userId = $request->get('userId');

        if ($request->has('userId')) {
            return Question::with('user')
                ->where('removed', '!=', true)
                ->where('user_id', $userId)->paginate(15);
        }

        return Question::with('user')->where('removed', '!=', true)->paginate(15);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Question::with('user')->where('removed', '!=', true)->findOrFail($id);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $this->validator($request);
        $imageName = Helpers::handleImageUpload($request);

        if ($validated === true) {
            $post = new Question();
            $post->title = $request->post('title');
            $post->content = $request->post('content');
            $post->image = $imageName;
            $post->category_id = intval($request->post('category'));
            $post->short_content = Str::limit($request->post('content'), 150, '...');
            $post->slug = Str::slug($request->post('title'), '-');
            $post->user_id = Auth::user()->id;
            $post->hidden_author = $request->post('hidden_author');
            $post->content_type = $request->post('content_type');
            $post->save();


            $modify = $post::with('user')->findOrFail($post->id);
            $modify['image'] = asset(env('PUBLIC_QUESTIONS_IMAGE_DIR') . $imageName);

            return $modify;
        } else {
            return response()->json([
                'status' => false,
                'errors' => $validated
            ]);
        }
    }

    private function validator(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'title' => 'required:min:20|max:150',
            'content' => 'required|min:250',
            'image' => 'required|image',
            'category' => 'required|exists:categories,id',
            'content_type' => 'required|in:0,1,2',
            'hidden_author' => 'required|boolean'
        ]);

        if ($validate->fails()) {
            return $validate->errors();
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::where('id', $id)->update([
            'removed' => true
        ]);

        return response()->json(['status' => 'success'], 200);
    }
}
