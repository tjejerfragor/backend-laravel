<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as UserModel;


class User extends Controller
{
    public function index()
    {
        return UserModel::with('user_details')->get();
    }
}
