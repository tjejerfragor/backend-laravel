<?php

use App\Comment;
use App\Follower;
use App\Post;
use App\PostLike;
use App\Question;
use App\UserLike;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Post::class, 1)->create();
        factory(Question::class, 1)->create();
        factory(Comment::class, 1)->create();
        factory(PostLike::class, 1)->create();
        factory(UserLike::class, 1)->create();
        factory(Follower::class, 1)->create();
    }
}
