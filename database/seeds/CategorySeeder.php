<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::
        table('categories')->insert([
            [
                'name' => "Ankäter",
                'description' => 'qweqweqwe',
                'slug' => "ankater",
                'image' => " "
            ],[
                'name' => "Nyheter",
                'description' => 'qweqweqwe',
                'slug' => "nyheter",
                'image' => " "
            ],[
                'name' => "Relations",
                'description' => 'qweqweqwe',
                'slug' => "relations",
                'image' => " "
            ],
        ]);
    }
}
