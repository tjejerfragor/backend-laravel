<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserLike;
use Faker\Generator as Faker;

$factory->define(UserLike::class, function (Faker $faker) {
    return [
        'liked' => $faker->boolean,
        'dissliked' => $faker->boolean(0),
        'question_id' => function () {
            return factory(App\Question::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        }
    ];
});
