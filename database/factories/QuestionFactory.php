<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(100),
        'content' => $faker->text,
        'short_content' => $faker->sentence,
        'slug' => $faker->slug,
        'image' => $faker->title,
        'category_id' => function () {
            return factory(App\Categories::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        }
    ];
});
