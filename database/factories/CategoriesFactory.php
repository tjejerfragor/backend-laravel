<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Categories;
use Faker\Generator as Faker;

$factory->define(Categories::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(10),
        'description' => $faker->text,
        'slug' => $faker->slug,
        'image' => $faker->sentence(10)

    ];
});
