<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PostLike;
use Faker\Generator as Faker;

$factory->define(PostLike::class, function (Faker $faker) {
    return [
        'female_like_count' => $faker->numberBetween(0,1000),
        'female_disslike_count' => $faker->numberBetween(0,1000),
        'male_like_count' => $faker->numberBetween(0,1000),
        'male_disslike_count' => $faker->numberBetween(0,1000),
        'question_id' => function() {
            return factory(App\Question::class)->create()->id;
        }
    ];
});
