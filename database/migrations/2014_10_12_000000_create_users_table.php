<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('about_me')->nullable();
            $table->string('relationship')->nullable();
            $table->string('career')->nullable();
            $table->string('city')->nullable();
            $table->string('profile_picture')->nullable();
            $table->string('main_picture')->nullable();
            $table->string('gender')->default('custom');
            $table->integer('user_level')->default(1);
            $table->boolean('hidden_profile')->default(false);
            $table->integer('first_steps')->default(1);
            $table->boolean('disabled')->default(false);
            $table->boolean('sponsored')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
