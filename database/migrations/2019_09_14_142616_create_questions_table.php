<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $content_types = [
            0 => "question",
            1 => "poll",
            2 => "news"
        ];

        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('content');
            $table->text('short_content');
            $table->smallInteger('content_type')->default(0);
            $table->string('slug');
            $table->string('image');
            $table->smallInteger('user_id');
            $table->smallInteger('category_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->boolean('disable_comments')->default(false);
            $table->boolean('disable_man')->default(false);
            $table->boolean('disable_woman')->default(false);
            $table->boolean('disable_anonymous')->default(false);
            $table->boolean('hidden_author')->default(false);
            $table->boolean('approved')->default(false);
            $table->boolean('editor_picked')->default(false);
            $table->boolean('removed')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
