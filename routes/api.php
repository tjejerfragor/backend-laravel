<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

header('Access-Control-Allow-Origin:  *');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')
    ->get('email/verify/{id}/{hash}', 'Api\AuthController@verifyEmail')
    ->name('verification.verify');

Route::prefix('v1')->group(function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');


    /**
     * Routes with authentication required
     */
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('user', 'Api\AuthController@getUser');
    });


    /**
     * Routes without authentication required
     */
    Route::get('/users', 'User@index');
    Route::resource('/posts', 'Posts');
    Route::resource('/questions', 'Questions');
    Route::get('/all', 'AllEntries@index');
    Route::resource('/comments', 'Comments');


    Route::prefix('/likes')->group(function () {
        Route::get('/', "LikesController@index");
        Route::post('/{id}', "LikesController@likeEntry");
        Route::delete('/{id}', "LikesController@dissLikeEntry");
    });
    Route::prefix('/followers')->group(function () {
        Route::get('/', "FollowersController@index");
        Route::post('/', "FollowersController@store");
        Route::delete('/', "FollowersController@destroy");
    });


});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
